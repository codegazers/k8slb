FROM nginx:alpine

LABEL maintainer="FRJARAUR/CODEGAZERS www.codegazers.org"

COPY k8slb.sh /docker-entrypoint.d/k8slb.sh

EXPOSE 6443