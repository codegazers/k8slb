Example usage:
```
docker run -d \
-e K8S_MANAGERS="192.168.201.131:6443,192.168.201.132:6443" \
--net=host 
--name k8slb \
codegazers/k8slb:1.0
```
