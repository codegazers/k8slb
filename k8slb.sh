#!/bin/sh -x
MANAGERS=${K8S_MANAGERS:-127.0.0.1:6443}
MANAGERS_ARRAY=$(echo $MANAGERS | tr "," " ")

EXPOSED_PORT=${EXPOSED_PORT:-6443}


[ "${MANAGERS_ARRAY}" == "" ] && exit

echo "Following backends have been configured listening on 6443:"

mv  /etc/nginx/nginx.conf /etc/nginx/nginx.conf.ORIG

cat <<-EOF >/etc/nginx/nginx.conf
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  multi_accept on;
  use epoll;
  worker_connections 1024;
}

stream {
    upstream k8s_masterservers {
EOF

for MANAGER in ${MANAGERS_ARRAY}
do
    echo "       server ${MANAGER} max_fails=2 fail_timeout=30s;" >>/etc/nginx/nginx.conf
done

cat <<-EOF >>/etc/nginx/nginx.conf
    }
    server {
        listen ${EXPOSED_PORT};
        proxy_pass k8s_masterservers;
        proxy_timeout 30;
        proxy_connect_timeout 2s;
    }
}
EOF